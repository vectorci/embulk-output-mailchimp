## 0.2.3 - 2016-03-24

- Retry when Mailchimp server return Internal Server Error [#11](https://github.com/treasure-data/embulk-output-mailchimp/pull/11)

## 0.2.2 - 2016-01-21

- Changed the threshold of batch size for uploading emails as a request [#9](https://github.com/treasure-data/embulk-output-mailchimp/pull/9)

## 0.2.1 - 2016-01-06

- Logging API request/response [#5](https://github.com/treasure-data/embulk-output-mailchimp/pull/5)
- Default `double_optin` to be `false` [#6](https://github.com/treasure-data/embulk-output-mailchimp/pull/6)
- Fixed when non-string groups given [#7](https://github.com/treasure-data/embulk-output-mailchimp/pull/7)
